﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadGenerateTriggerControl : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameControl.Instance.SpawnNextRoadPart(this.transform);
    }
}
