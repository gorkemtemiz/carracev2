﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData
{
    public string Name;
    public int Age;
    public string Country;
    public int PlayerAvatarIndex;
    public int CarIndex;
    public int Score;
    public int correctAnswerCount;
    public int wrongAnswerCount;
}
