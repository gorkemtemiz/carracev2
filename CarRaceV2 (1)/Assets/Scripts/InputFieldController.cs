﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InputFieldController : MonoBehaviour
{
    public TMP_InputField _InputField;
    private string lastText;

    public void HideTheText()
    {
        lastText = _InputField.placeholder.transform.GetComponent<TMPro.TextMeshProUGUI>().text;
        _InputField.placeholder.transform.GetComponent<TMPro.TextMeshProUGUI>().SetText("");
    }

    public void ShowTheText()
    {
        _InputField.placeholder.transform.GetComponent<TMPro.TextMeshProUGUI>().SetText(lastText);
    }
}
