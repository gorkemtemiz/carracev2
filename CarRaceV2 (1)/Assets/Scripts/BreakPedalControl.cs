﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class BreakPedalControl : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    private bool pointerButtonDown; // it becomes true, when a player press down on the break pedal.

    private void Start()
    {
        pointerButtonDown = false;
    }

    void Update()
    {
        if (GameControl.Instance.startTheGame && pointerButtonDown)
        {
            GameControl.Instance.ShowBreakFX();
            if (GameControl.Instance.carCurrentSpeed > GameControl.Instance.carMinSpeed)
            {
                GameControl.Instance.carCurrentSpeed -= GameControl.Instance.carBreakNegativeAcceleration;
            }
        }
    }


    public void OnPointerDown(PointerEventData eventData)
    {
        pointerButtonDown = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        pointerButtonDown = false;
    }
}
