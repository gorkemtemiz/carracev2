﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public GameObject Car;
    private float carPosY;
   
    void Update()
    {
        carPosY = Car.transform.position.y;
        if (Car.activeSelf)
            transform.position = new Vector3(0, carPosY + 40, transform.position.z);
    }
}
