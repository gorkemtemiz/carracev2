﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoControl : MonoBehaviour
{
    public GameObject CurrentUI;
    public GameObject NextUI;

    [HideInInspector] public double totalTime;
    [HideInInspector] public double currentTime;



    void Start()
    {
        totalTime = gameObject.GetComponent<VideoPlayer>().clip.length;
    }


    void Update()
    {
        currentTime = gameObject.GetComponent<VideoPlayer>().time;
        if(currentTime >= totalTime)
        {
            Debug.Log("video durdu!");
            gameObject.GetComponent<VideoPlayer>().Stop();
            CurrentUI.SetActive(false);
            NextUI.SetActive(true);
        }
    }
}
