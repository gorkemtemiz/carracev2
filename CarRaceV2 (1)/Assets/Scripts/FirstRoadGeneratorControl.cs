﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstRoadGeneratorControl : MonoBehaviour
{
    public GameObject roadPrefab;
    public GameObject parentRoad;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        SpawnNextRoadPart(this.transform);
    }

    public void SpawnNextRoadPart(Transform givenTransform)
    {
        Instantiate(roadPrefab, new Vector3(0, givenTransform.position.y + 1000, 0), Quaternion.identity, parentRoad.transform);
    }
}
