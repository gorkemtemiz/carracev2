﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.UI;

public class AnswerTriggerControl : MonoBehaviour
{
    public QuestionTriggerControl qtControl;
    [HideInInspector] public int choice;
    [HideInInspector] public int indexOfChoice;

    private void OnTriggerEnter2D(Collider2D collision)
    {

        indexOfChoice = qtControl.answersGameObjectList.IndexOf(gameObject);
        choice = qtControl.choices[indexOfChoice];


        if (choice == qtControl.correctAnswer)
        {
            StartCoroutine(TriggerAnswerLight(true));
            UIControl.Instance.playerData.correctAnswerCount++;
            UIControl.Instance.playerData.Score += 50;
            UIControl.Instance.inGameScoreTextObject.GetComponentInChildren<TMPro.TextMeshProUGUI>().SetText(UIControl.Instance.playerData.Score.ToString());
            UIControl.Instance.endGameCorrectAnswerText.GetComponentInChildren<TMPro.TextMeshProUGUI>().SetText(UIControl.Instance.playerData.correctAnswerCount.ToString());
        }
        if (choice != qtControl.correctAnswer)
        {
            StartCoroutine(TriggerAnswerLight(false));
            UIControl.Instance.playerData.wrongAnswerCount++;
            UIControl.Instance.playerData.Score -= 5;
            UIControl.Instance.inGameScoreTextObject.GetComponentInChildren<TMPro.TextMeshProUGUI>().SetText(UIControl.Instance.playerData.Score.ToString());
            UIControl.Instance.endGameWrongAnswerText.GetComponentInChildren<TMPro.TextMeshProUGUI>().SetText(UIControl.Instance.playerData.wrongAnswerCount.ToString());
        }
    }


    IEnumerator TriggerAnswerLight(bool isTrue)
    {
        if (isTrue && !UIControl.Instance.isAnswerTriggered)
        {
            UIControl.Instance.isAnswerTriggered = true;
            UIControl.Instance.correctAnswer_AS.Play();
            int i = 0;
            while(i < 3)
            {
                UIControl.Instance.table_Answers[indexOfChoice].transform.GetChild(0).GetComponent<Image>().DOFade(1f, 0.15f).SetEase(Ease.InOutCubic);
                yield return new WaitForSeconds(0.2f);
                UIControl.Instance.table_Answers[indexOfChoice].transform.GetChild(0).GetComponent<Image>().DOFade(0f, 0.15f).SetEase(Ease.InOutCubic);
                yield return new WaitForSeconds(0.2f);
                i++;
            }
            UIControl.Instance.table_Question.GetComponent<TMPro.TMP_Text>().SetText("");
            UIControl.Instance.table_Answers[0].GetComponent<TMPro.TMP_Text>().SetText("");
            UIControl.Instance.table_Answers[1].GetComponent<TMPro.TMP_Text>().SetText("");
            UIControl.Instance.table_Answers[2].GetComponent<TMPro.TMP_Text>().SetText("");
            UIControl.Instance.isAnswerTriggered = false;
        }
        if (!isTrue && !UIControl.Instance.isAnswerTriggered)
        {
            UIControl.Instance.isAnswerTriggered = true;
            UIControl.Instance.wrongAnswer_AS.Play();
            int i = 0;
            while (i < 3)
            {
                UIControl.Instance.table_Answers[indexOfChoice].transform.GetChild(1).GetComponent<Image>().DOFade(1f, 0.15f).SetEase(Ease.InOutCubic);
                yield return new WaitForSeconds(0.2f);
                UIControl.Instance.table_Answers[indexOfChoice].transform.GetChild(1).GetComponent<Image>().DOFade(0f, 0.15f).SetEase(Ease.InOutCubic);
                yield return new WaitForSeconds(0.2f);
                i++;
            }
            UIControl.Instance.table_Question.GetComponent<TMPro.TMP_Text>().SetText("");
            UIControl.Instance.table_Answers[0].GetComponent<TMPro.TMP_Text>().SetText("");
            UIControl.Instance.table_Answers[1].GetComponent<TMPro.TMP_Text>().SetText("");
            UIControl.Instance.table_Answers[2].GetComponent<TMPro.TMP_Text>().SetText("");
            UIControl.Instance.isAnswerTriggered = false;
        }
    }
}
