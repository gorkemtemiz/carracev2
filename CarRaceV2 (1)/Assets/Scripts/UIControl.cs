﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class UIControl : Singleton<UIControl>
{
    #region ------ public editor parameters ------
    [Header("Main Screen Objects")]
    public GameObject welcomePageUI;
    public GameObject gameInfoUI;
    public GameObject logInUI;
    public GameObject avatarSelectionUI;
    public GameObject carSelectionUI;
    public GameObject numberSelectionUI;
    public GameObject controlInfoUI;
    public GameObject carIntroUI;
    public GameObject inGameUI;
    public GameObject gameEnvironment;
    public GameObject pauseGameUI;
    public GameObject endGameUI;

    [Header("Audio Elements")]
    public AudioSource mainTheme_AS;
    public AudioSource introVideo_AS;
    public AudioSource break_AS;
    public AudioSource engine_AS;
    public AudioSource correctAnswer_AS;
    public AudioSource wrongAnswer_AS;
    public AudioSource buttonNext_AS;
    public AudioSource buttonSlide_AS;
    public AudioSource buttonNumber_AS;
    public AudioSource countdown123_AS;
    public AudioSource countdownGO_AS;
    public List<GameObject> SoundButtons;
    public Sprite soundButton_On;
    public Sprite soundButton_Off;

    [Header("Welcome Page Screen Objects")]
    public GameObject gameNameBanner;
    public GameObject welcomePageNextButton;

    [Header("Game Info Screen Objects")]
    public GameObject gameInfoNextButton;

    [Header("Log In Screen Objects")]
    public GameObject loginNameInputField; // InputField object for the name of the player, It's in the PlayerLogInUI
    public GameObject loginAgeInputField; // InputField object for the age of the player, It's in the PlayerLogInUI
    public GameObject loginPageNextButton;

    [Header("Avatar Selection Screen Objects")]
    public RectTransform avatarPanel;
    public GameObject avatarLeftButton;
    public GameObject avatarRightButton;
    public GameObject avatarNextButton;

    [Header("Car Selection Screen Objects")]
    public RectTransform carPanel;
    public GameObject carLeftButton;
    public GameObject carRightButton;
    public GameObject carNextButton;

    [Header("Number Selection Screen Objects")]
    public GameObject numberSelectionPanel;
    public GameObject numberSelectionNextButton;
    public GameObject[] numberSelectionLevelButtons;

    [Header("In-Game Screen Objects")]
    public GameObject car;
    public GameObject inGameCountdownTextObject; // UI text gameobject in UIManager, Its name is Countdown_Text
    public GameObject inGameScoreTextObject;
    public GameObject inGameSpeedTextObject;
    public GameObject avatarImageObject;
    public GameObject inGamePauseButton;
    public GameObject inGameSoundButton;
    public GameObject inGameBreakPedalButton;
    public GameObject table_Question;
    public GameObject[] table_Answers;
    public float sessionCountdown; // the remaining time counter to play
    public Sprite[] avatarSprites;
    public Sprite[] carSprites;

    public GameObject[] startCoundownAnimObjs;

    [Header("Pause Screen Objects")]
    public GameObject pauseHomeButton;
    public GameObject pauseCloseButton;
    public GameObject pauseRestartButton;

    [Header("End Game Screen Objects")]
    public GameObject endGameNameText;
    public GameObject endGameAgeText;
    public GameObject endGameCorrectAnswerText;
    public GameObject endGameWrongAnswerText;
    public GameObject endGameScoreText;
    public GameObject endGameRestartButton;
    public GameObject endGameHomeButton;
    public GameObject avatarAnimations;
    #endregion

    #region ------ private & hidden objects ------
    [HideInInspector] public PlayerData playerData; // PLAYER DATA OBJECT
    [HideInInspector] public bool isCountingDown; // the boolean control to start/stop for counting down
    [HideInInspector] public bool startTheGame;
    [HideInInspector] public bool isAnswerTriggered;


    private int avatarClickCounter;
    private int carClickCounter;
    private bool isAvatarSelectionButtonClicked;
    private bool isCarSelectionButtonClicked;
    private bool isStartAnimationRunning;
    private Vector2 welcomeRect;
    private Vector2 gameInfoRect;
    private Vector2 logInRect;
    private Vector2 avatarSelectRect;
    private Vector2 carSelectRect;
    private Vector2 numberSelectRect;
    private Vector2 controlInfoRect;
    private Vector2 pauseRect;
    private Vector2 endGameRect;
    #endregion


    #region ------ MonoBehaviour Functions ------
    private void Awake()
    {
        startTheGame = false;
        isAnswerTriggered = false;
        playerData = new PlayerData
        {
            Score = 0,
            correctAnswerCount = 0,
            wrongAnswerCount = 0
        };

        welcomeRect = welcomePageUI.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().anchoredPosition;
        gameInfoRect = gameInfoUI.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().anchoredPosition;
        logInRect = logInUI.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().anchoredPosition;
        avatarSelectRect = avatarSelectionUI.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().anchoredPosition;
        carSelectRect = carSelectionUI.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().anchoredPosition;
        numberSelectRect = numberSelectionUI.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().anchoredPosition;
        controlInfoRect = controlInfoUI.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().anchoredPosition;
        pauseRect = pauseGameUI.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().anchoredPosition;
        endGameRect = endGameUI.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().anchoredPosition;
    }

    void Start()
    {
        avatarClickCounter = 0;
        carClickCounter = 0;
        isCountingDown = true;
        isAvatarSelectionButtonClicked = false;
        isCarSelectionButtonClicked = false;
        isStartAnimationRunning = false;

        welcomePageUI.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().DOAnchorPos(Vector2.zero, 1f).SetEase(Ease.OutQuint);
        BounceNameBanner();
    }

    void Update()
    {
        if (startTheGame)
        {
            CountdownTimer();
            inGameSpeedTextObject.transform.GetComponentInChildren<TMPro.TextMeshProUGUI>().SetText(Mathf.FloorToInt(GameControl.Instance.carCurrentSpeed * 40).ToString());
        }

        if (endGameUI.activeSelf)
        {
            endGameNameText.transform.GetComponentInChildren<TMPro.TextMeshProUGUI>().SetText(playerData.Name);
            endGameAgeText.transform.GetComponentInChildren<TMPro.TextMeshProUGUI>().SetText(playerData.Age.ToString());
            endGameCorrectAnswerText.transform.GetComponentInChildren<TMPro.TextMeshProUGUI>().SetText(playerData.correctAnswerCount.ToString());
            endGameWrongAnswerText.transform.GetComponentInChildren<TMPro.TextMeshProUGUI>().SetText(playerData.wrongAnswerCount.ToString());
            endGameScoreText.transform.GetComponentInChildren<TMPro.TextMeshProUGUI>().SetText(playerData.Score.ToString());
        }
    }
    #endregion


    #region ------ common functions ------
    public void StartTheGame()
    {
        gameEnvironment.SetActive(true);
        startTheGame = true;
        GameControl.Instance.startTheGame = true;
        mainTheme_AS.volume = 0.3f;
        engine_AS.Play();
    }

    public void StopTheGame()
    {
        break_AS.Stop();
        engine_AS.Stop();
        mainTheme_AS.volume = 0.75f;
        gameEnvironment.SetActive(false);
        startTheGame = false;
        GameControl.Instance.startTheGame = false;
    }

    public void RestartTheGame(GameObject currentUI, GameObject nextUI)
    {
        StopCountdown();
        StopTheGame();

        for (int i = 5; i < GameControl.Instance.roadsTransforms.Length; i++)
        {
            Destroy(GameControl.Instance.roadsTransforms[i].gameObject);
        }

        GameControl.Instance.car.transform.position = new Vector3(0, 50, 0);
        GameControl.Instance.carCurrentSpeed = GameControl.Instance.carMinSpeed;
        sessionCountdown = 60;
        playerData.Score = 0;
        inGameCountdownTextObject.transform.GetComponentInChildren<TMPro.TextMeshProUGUI>().SetText("00 " + sessionCountdown.ToString());
        inGameScoreTextObject.transform.GetComponentInChildren<TMPro.TextMeshProUGUI>().SetText(playerData.Score.ToString());
        inGameSpeedTextObject.transform.GetComponentInChildren<TMPro.TextMeshProUGUI>().SetText("0");
        nextUI.SetActive(true);
        currentUI.SetActive(false);
        AnimateStartCountdown();
        StartCountdown();
    }

    public void ReturnHome()
    {
        StopCountdown();
        StopTheGame();

        for (int i = 5; i < GameControl.Instance.roadsTransforms.Length; i++)
        {
            Destroy(GameControl.Instance.roadsTransforms[i].gameObject);
        }

        GameControl.Instance.car.transform.position = new Vector3(0, 50, 0);
        GameControl.Instance.carCurrentSpeed = GameControl.Instance.carMinSpeed;
        sessionCountdown = 60;
        playerData.Score = 0;
        inGameCountdownTextObject.transform.GetComponentInChildren<TMPro.TextMeshProUGUI>().SetText("00 " + sessionCountdown.ToString());
        inGameScoreTextObject.transform.GetComponentInChildren<TMPro.TextMeshProUGUI>().SetText(playerData.Score.ToString());
        inGameSpeedTextObject.transform.GetComponentInChildren<TMPro.TextMeshProUGUI>().SetText("0");
    }

    public void StartCountdown()
    {
        isCountingDown = true;
    }

    public void StopCountdown()
    {
        isCountingDown = false;
    }

    void CountdownTimer()
    {
        if (isCountingDown)
        {
            sessionCountdown -= Time.deltaTime; // the operation which decreases the time

            //the operation which set the text of time counter
            inGameCountdownTextObject.transform.GetComponentInChildren<TMPro.TextMeshProUGUI>().SetText("00 " + ((int)sessionCountdown).ToString());

            if (sessionCountdown <= 0)
            {
                StopTheGame();
                StopCountdown();
                SetDataToEndGameScreen();
                SlideForwardTheUIPage(inGameUI, endGameUI);
            }
        }
    }

    public void RegisterPlayer()
    {
        playerData.Name = loginNameInputField.transform.GetChild(0).GetComponent<TMPro.TMP_InputField>().text;
        playerData.Age = int.Parse(loginAgeInputField.transform.GetChild(0).GetComponent<TMPro.TMP_InputField>().text);
        playerData.Country = "Turkey"; // This may be changed, depends on a request
        playerData.Score = 0;
    }

    public void ChangeTheScreen(GameObject current, GameObject next)
    {
        current.SetActive(false);
        next.SetActive(true);
    }

    public void SlideForwardTheUIPage(GameObject currentPage, GameObject nextPage)
    {
        nextPage.SetActive(true);
        SetToInitialPosOfUI(currentPage);
        currentPage.SetActive(false);
        nextPage.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().DOAnchorPos(Vector2.zero, 0.75f).SetEase(Ease.OutQuint);
    }

    private void SetToInitialPosOfUI(GameObject obj)
    {
        switch (obj.name)
        {
            case "WelcomePageUI":
                welcomePageUI.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().anchoredPosition = welcomeRect;
                break;
            case "GameInfoUI":
                gameInfoUI.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().anchoredPosition = gameInfoRect;
                break;
            case "LogInUI":
                logInUI.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().anchoredPosition = logInRect;
                break;
            case "AvatarSelectionUI":
                avatarSelectionUI.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().anchoredPosition = avatarSelectRect;
                break;
            case "CarSelectionUI":
                carSelectionUI.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().anchoredPosition = carSelectRect;
                break;
            case "NumberSelectionUI":
                numberSelectionUI.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().anchoredPosition = numberSelectRect;
                break;
            case "ControlInfoUI":
                controlInfoUI.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().anchoredPosition = controlInfoRect;
                break;
            case "PauseGameUI":
                pauseGameUI.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().anchoredPosition = pauseRect;
                break;
            case "EndGameUI":
                endGameUI.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().anchoredPosition = endGameRect;
                break;
        }
    }

    public void MuteSounds()
    {
        AudioListener.pause = !AudioListener.pause;
        if (AudioListener.pause)
        {
            foreach (var button in SoundButtons)
            {
                button.GetComponent<Image>().sprite = soundButton_Off;
            }
        }
        else
        {
            EventSystem.current.currentSelectedGameObject.GetComponent<Image>().sprite = soundButton_On;
            foreach (var button in SoundButtons)
            {
                button.GetComponent<Image>().sprite = soundButton_On;
            }
        }
    } 

    IEnumerator WaitAWhile(float sec)
    {
        yield return new WaitForSecondsRealtime(sec);
    }

    public void SetDataToEndGameScreen()
    {
        endGameNameText.transform.GetChild(0).GetComponent<TMPro.TMP_Text>().SetText(playerData.Name);
        endGameAgeText.transform.GetChild(0).GetComponent<TMPro.TMP_Text>().SetText(playerData.Age.ToString());
        endGameScoreText.transform.GetChild(0).GetComponent<TMPro.TMP_Text>().SetText(playerData.Score.ToString());
        endGameCorrectAnswerText.transform.GetChild(0).GetComponent<TMPro.TMP_Text>().SetText(playerData.correctAnswerCount.ToString());
        endGameWrongAnswerText.transform.GetChild(0).GetComponent<TMPro.TMP_Text>().SetText(playerData.wrongAnswerCount.ToString());
    }

    #endregion

    #region ------ welcome page functions ------
    public void GoNextPage_welcome()
    {
        buttonNext_AS.Play();
        SlideForwardTheUIPage(welcomePageUI, gameInfoUI);
    }

    public void BounceNameBanner()
    {
        StartCoroutine(BounceNameBanner_Coroutine());
    }

    IEnumerator BounceNameBanner_Coroutine()
    {
        yield return new WaitForSeconds(3f);
        while (gameNameBanner.activeInHierarchy)
        {
            gameNameBanner.transform.DOScale(1.05f, 0.5f).SetEase(Ease.InOutCubic);
            yield return new WaitForSeconds(0.6f);
            gameNameBanner.transform.DOScale(1f, 0.5f).SetEase(Ease.InOutCirc);
            yield return new WaitForSeconds(0.6f);
        }
    }
    #endregion

    #region ------ game info page functions ------
    public void GoNextPage_gameInfo()
    {
        buttonNext_AS.Play();
        SlideForwardTheUIPage(gameInfoUI, logInUI);
    }
    #endregion

    #region ------ player login page functions ------
    public void GoNextPage_logIn()
    {
        buttonNext_AS.Play();
        RegisterPlayer();
        SlideForwardTheUIPage(logInUI, avatarSelectionUI);
    }
    #endregion

    #region ------ avatar selection page functions ------
    public void SlideAvatarsToLeft() // This is attached to Right_Button in the AvatarSelectionUI
    {
        StartCoroutine(SlideAvatarsToLeft_Coroutine());
    }

    IEnumerator SlideAvatarsToLeft_Coroutine()
    {
        if (!isAvatarSelectionButtonClicked)
        {
            isAvatarSelectionButtonClicked = true;
            if (avatarClickCounter >= 0 && avatarClickCounter < 2)
            {
                avatarRightButton.SetActive(true);
                avatarLeftButton.SetActive(true);
                avatarPanel.DOLocalMoveX(avatarPanel.localPosition.x - 2000, 0.5f).SetEase(Ease.OutCirc);
                avatarClickCounter++;
                if (avatarClickCounter == 2)
                {
                    avatarRightButton.SetActive(false);
                }
                buttonSlide_AS.Play();
                yield return new WaitForSeconds(0.6f);
            }

            if (avatarClickCounter > 2)
            {
                avatarClickCounter = 2;
            }

            isAvatarSelectionButtonClicked = false;
        }
    }

    public void SlideAvatarsToRight() // This is attached to Left_Button in the AvatarSelectionUI
    {
        StartCoroutine(SlideAvatarsToRight_Coroutine());
    }

    IEnumerator SlideAvatarsToRight_Coroutine()
    {
        if (!isAvatarSelectionButtonClicked)
        {
            isAvatarSelectionButtonClicked = true;
            if (avatarClickCounter > 0 && avatarClickCounter <= 2)
            {
                avatarRightButton.SetActive(true);
                avatarLeftButton.SetActive(true);
                avatarPanel.DOLocalMoveX(avatarPanel.localPosition.x + 2000, 0.5f).SetEase(Ease.OutCirc);
                avatarClickCounter--;
                if (avatarClickCounter == 0)
                {
                    avatarLeftButton.SetActive(false);
                }
                buttonSlide_AS.Play();
                yield return new WaitForSeconds(0.6f);
            }

            if (avatarClickCounter < 0)
            {
                avatarClickCounter = 0;
            }
            isAvatarSelectionButtonClicked = false;
        }
    }

    public void SetTheAvatar()
    {
        int avatarIndex = avatarClickCounter;
        playerData.PlayerAvatarIndex = avatarIndex;
        avatarImageObject.transform.GetComponent<Image>().sprite = avatarSprites[avatarIndex];
        avatarAnimations.transform.GetChild(playerData.PlayerAvatarIndex).gameObject.SetActive(true);
    }

    public void GoNextPage_avatar() // This is attached to Next_Button in the AvatarSelectionUI
    {
        buttonNext_AS.Play();
        SetTheAvatar();
        SlideForwardTheUIPage(avatarSelectionUI, carSelectionUI);
    }
    #endregion

    #region ------ car selection page functions ------
    public void SlideCarsToLeft()
    {
        StartCoroutine(SlideCarsToLeft_Coroutine());
    }

    IEnumerator SlideCarsToLeft_Coroutine()
    {
        if (!isCarSelectionButtonClicked)
        {
            isCarSelectionButtonClicked = true;
            if (carClickCounter >= 0 && carClickCounter < 3)
            {
                carLeftButton.SetActive(true);
                carRightButton.SetActive(true);
                carPanel.DOLocalMoveX(carPanel.localPosition.x - 2000, 0.5f).SetEase(Ease.OutCirc);
                carClickCounter++;
                if (carClickCounter == 3)
                {
                    carRightButton.SetActive(false);
                }
                buttonSlide_AS.Play();
                yield return new WaitForSeconds(0.6f);
            }

            if (carClickCounter > 3)
            {
                carClickCounter = 3;
            }
            
            isCarSelectionButtonClicked = false;
        }
    }

    public void SlideCarsToRight()
    {
        StartCoroutine(SlideCarsToRight_Coroutine());
    }

    IEnumerator SlideCarsToRight_Coroutine()
    {
        if (!isCarSelectionButtonClicked)
        {
            isCarSelectionButtonClicked = true;
            if (carClickCounter > 0 && carClickCounter <= 3)
            {
                carLeftButton.SetActive(true);
                carRightButton.SetActive(true);
                carPanel.DOLocalMoveX(carPanel.localPosition.x + 2000, 0.5f).SetEase(Ease.OutCirc);
                carClickCounter--;
                if (carClickCounter == 0)
                {
                    carLeftButton.SetActive(false);
                }
                buttonSlide_AS.Play();
                yield return WaitAWhile(0.6f);
            }

            if (carClickCounter < 0)
            {
                carClickCounter = 0;
                yield return WaitAWhile(1.1f);
            }

            isCarSelectionButtonClicked = false;
        }
    }

    public void SetTheCar()
    {
        int carIndex = carClickCounter;
        playerData.CarIndex = carIndex;
        car.transform.GetComponent<SpriteRenderer>().sprite = GameControl.Instance.carSprites[playerData.CarIndex];
    }

    public void GoNextPage_car()
    {
        buttonNext_AS.Play();
        SetTheCar();
        SlideForwardTheUIPage(carSelectionUI, numberSelectionUI);
    }
    #endregion

    #region ------ number selection page functions ------
    public void SetTheNumber()
    {
        string buttonName = EventSystem.current.currentSelectedGameObject.name;
        if (buttonName != null)
        {
            buttonNumber_AS.Play();
            GameControl.Instance.chosenLevel = buttonName;
            numberSelectionNextButton.SetActive(true);
        }
    }

    public void GoNextPage_number()
    {
        buttonNext_AS.Play();
        SlideForwardTheUIPage(numberSelectionUI, controlInfoUI);
    }
    #endregion

    #region ------ control info page functions ------
    public void GoNextPage_controlInfo()
    {
        buttonNext_AS.Play();
        ChangeTheScreen(controlInfoUI, carIntroUI);
    }
    #endregion

    #region ------ car intro page functions ------
    public void GoNextPage_carIntro()
    {
        ChangeTheScreen(carIntroUI, inGameUI);
        AnimateStartCountdown();
    }
    #endregion

    #region ------ in-game page functions ------
     public void AnimateStartCountdown()
     {
        StartCoroutine(AnimateStartCountdown_Coroutine());
     }

    IEnumerator AnimateStartCountdown_Coroutine()
    {
        isStartAnimationRunning = true;
        gameEnvironment.SetActive(true);
        for (int i = 0; i < 4; i++)
        {
            startCoundownAnimObjs[i].SetActive(true);
            if(i <= 2)
            {
                countdown123_AS.Play();
            }
            if (i == 3)
            {
                countdownGO_AS.Play();
            }
            startCoundownAnimObjs[i].transform.DOScale(2.7f, 1f).SetEase(Ease.OutQuad);
            yield return new WaitForSeconds(1.1f);
            startCoundownAnimObjs[i].SetActive(false);
        }

        StartTheGame();
        StartCountdown();
        for (int i = 0; i < 4; i++)
        {
            startCoundownAnimObjs[i].transform.localScale = Vector3.one;
        }

        isStartAnimationRunning = false;
    }

    public void PauseTheGame()
    {
        if (!isStartAnimationRunning)
        {
            buttonNext_AS.Play();
            StopTheGame();
            SlideForwardTheUIPage(inGameUI, pauseGameUI);
        }
    }
    #endregion

    #region ------ pause game page region ------
    public void ReturnToHome_pause()
    {
        buttonNext_AS.Play();
        ReturnHome();
        SlideForwardTheUIPage(pauseGameUI, welcomePageUI);
    }

    public void ContinueTheGame()
    {
        buttonNext_AS.Play();

        inGameUI.SetActive(true);
        pauseGameUI.SetActive(false);
        StartTheGame();
    }

    public void RestartTheGame_pause()
    {
        buttonNext_AS.Play();

        RestartTheGame(pauseGameUI, inGameUI);
    }

    #endregion

    #region ------ end-game page region ------
    public void ReturnToHome_endGame()
    {
        buttonNext_AS.Play();
        ReturnHome();
        SlideForwardTheUIPage(endGameUI, welcomePageUI);
    }

    public void RestartTheGame_endGame()
    {
        buttonNext_AS.Play();
        RestartTheGame(endGameUI, inGameUI);
    }
    #endregion

}
