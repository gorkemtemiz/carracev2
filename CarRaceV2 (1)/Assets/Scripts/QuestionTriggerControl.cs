﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using Random = UnityEngine.Random;
using UnityEngine.UI;

public class QuestionTriggerControl : MonoBehaviour
{
    // this array always has 3 elements, 2 of them are randomly generated, and 1 is the correct choice.
    public List<GameObject> answersGameObjectList;

    [HideInInspector] public int multiplier_1;  // 1st multiplier of the questions
    [HideInInspector] public int multiplier_2;  // 2nd multipler of the questions
    [HideInInspector] public int correctAnswer; // the answer of the random generated question
    [HideInInspector] public List<int> choices;


    private void Start()
    {
        choices = new List<int>(3);

        // multipliers are selected depending on the choice at the number selection screen.
        if (GameControl.Instance.chosenLevel != null)
        {
            switch (GameControl.Instance.chosenLevel)
            {
                case "1":
                    multiplier_1 = 1;
                    multiplier_2 = Random.Range(1, 10);
                    break;
                case "2":
                    multiplier_1 = 2;
                    multiplier_2 = Random.Range(1, 10);
                    break;
                case "3":
                    multiplier_1 = 3;
                    multiplier_2 = Random.Range(1, 10);
                    break;
                case "4":
                    multiplier_1 = 4;
                    multiplier_2 = Random.Range(1, 10);
                    break;
                case "5":
                    multiplier_1 = 5;
                    multiplier_2 = Random.Range(1, 10);
                    break;
                case "6":
                    multiplier_1 = 6;
                    multiplier_2 = Random.Range(1, 10);
                    break;
                case "7":
                    multiplier_1 = 7;
                    multiplier_2 = Random.Range(1, 10);
                    break;
                case "8":
                    multiplier_1 = 8;
                    multiplier_2 = Random.Range(1, 10);
                    break;
                case "9":
                    multiplier_1 = 9;
                    multiplier_2 = Random.Range(1, 10);
                    break;
                case "All":
                    multiplier_1 = Random.Range(1, 10);
                    multiplier_2 = Random.Range(1, 10);
                    break;
            }
        }

        // to set the text of the question object. It can be found in Question objects at the "Road" PreFabs.
        gameObject.GetComponent<TMPro.TMP_Text>().SetText(multiplier_1 + "   x   " + multiplier_2);

        // to calculate the real answer of the randomly generated question. 
        correctAnswer = multiplier_1 * multiplier_2; 
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        int answerIndex = Random.Range(0, 3); // to randomize the permutation of the answer choices. 
        
        UIControl.Instance.table_Question.GetComponent<TMPro.TMP_Text>().SetText(multiplier_1 + " x " + multiplier_2);
        StartCoroutine(TriggerQuestionLight());
        // The loop below sets the texts of the permutation of the choices with respect to answerIndex. 
        for (int i = 0; i < 3; i++)
        {
            if (i == answerIndex)
            {
                choices.Add(correctAnswer);
                answersGameObjectList[i].GetComponent<TMPro.TMP_Text>().SetText(correctAnswer.ToString());
                UIControl.Instance.table_Answers[i].GetComponent<TMPro.TMP_Text>().SetText(correctAnswer.ToString());
            }
            if (i != answerIndex)
            {
                int randomNumber = Random.Range(1, 100);
                choices.Add(randomNumber);
                answersGameObjectList[i].GetComponent<TMPro.TMP_Text>().SetText(randomNumber.ToString());
                UIControl.Instance.table_Answers[i].GetComponent<TMPro.TMP_Text>().SetText(randomNumber.ToString());
            }
        }
    }

    IEnumerator TriggerQuestionLight()
    {
        int i = 0;
        while(i < 6)
        {
            UIControl.Instance.table_Question.transform.GetChild(0).GetComponent<Image>().DOFade(1f, 0.1f).SetEase(Ease.InOutCubic);
            yield return new WaitForSeconds(0.2f);
            UIControl.Instance.table_Question.transform.GetChild(0).GetComponent<Image>().DOFade(0f, 0.1f).SetEase(Ease.InOutCubic);
            yield return new WaitForSeconds(0.2f);
            i++;
        }

    }
}
