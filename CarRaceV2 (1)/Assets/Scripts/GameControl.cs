﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

public class GameControl : Singleton<GameControl>
{
    [Header("Gameplay Objects")]
    public GameObject gameEnvironment;
    public GameObject roadPrefab;
    public GameObject parentRoad;
    public GameObject car;
    public GameObject breakFXObject;
    public GameObject flameFXObject;
    public Sprite[] carSprites;

    [Header("Gameplay Parameters")]
    public float carCurrentSpeed;
    public float carMinSpeed;
    public float carMaxSpeed;
    public float carAcceleration;
    public float carBreakNegativeAcceleration;


    [HideInInspector] public bool startTheGame;
    [HideInInspector] public string chosenLevel; // selected level at the number selection page ui
    [HideInInspector] public Transform[] roadsTransforms; // the array of the children of Roads Game Object
    private readonly Ease ease = Ease.OutBack;
    private float currentCarPosY;
    [HideInInspector] public bool isFlameBeingShowed = false;
    [HideInInspector] public bool isCarMovingHorizontally = false;


    //private Vector3 deltaPosition;
    //private Vector3 mouseLastPosition;

    private void Start()
    {
        startTheGame = false;
        //deltaPosition = new Vector3();
        //mouseLastPosition = new Vector3();
    }

    void Update()
    {
        if (gameEnvironment.activeInHierarchy && startTheGame)
        {
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (car.transform.position.x < 65f)
                {
                    StartCoroutine(MoveTheCar("right"));
                }
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if (car.transform.position.x > -65f)
                {
                    StartCoroutine(MoveTheCar("left"));
                }
            }

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (isFlameBeingShowed)
                {
                    HideFlameFX();
                }
                UIControl.Instance.engine_AS.Stop();
                UIControl.Instance.break_AS.Play();
                ShowBreakFX();
            }

            if (Input.GetKey(KeyCode.DownArrow) && carCurrentSpeed > carMinSpeed)
            {
                carCurrentSpeed -= carBreakNegativeAcceleration;          
            }

            if (!Input.GetKey(KeyCode.DownArrow) && carCurrentSpeed <= carMaxSpeed)
            {
                if (!isFlameBeingShowed)
                {
                    ShowFlameFX();
                }

                flameFXObject.transform.GetComponent<Animator>().Play("FlameFXAnim", 0);
                carCurrentSpeed += carAcceleration;
            }

            if (Input.GetKeyUp(KeyCode.DownArrow) && !UIControl.Instance.engine_AS.isPlaying)
            {
                UIControl.Instance.engine_AS.Play();
            }

            car.transform.position = new Vector3(car.transform.position.x, car.transform.position.y 
                                                + Mathf.Clamp(carCurrentSpeed, carMinSpeed, carMaxSpeed), 0);

            #region ------ mouse && touch controls ------
            //if (Input.GetMouseButtonDown(0))
            //{
            //    mouseLastPosition = Input.mousePosition;
            //}

            //if (Input.GetMouseButtonUp(0)
            //{
            //    deltaPosition = Input.mousePosition - mouseLastPosition;

            //    if (deltaPosition.x > 0 && car.transform.position.x < 45f)
            //    {
            //        car.transform.DOMoveX(Mathf.Clamp(car.transform.position.x + 44f, -42f, 46f), 0.75f).SetEase(ease);
            //    }
            //    if (deltaPosition.x < 0 && car.transform.position.x > -41f)
            //    {
            //        car.transform.DOMoveX(Mathf.Clamp(car.transform.position.x - 44f, -42f, 46f), 0.5f).SetEase(ease);
            //    }
            //}
            #endregion
        }

        DestroyPastRoadPart();    
    }

    IEnumerator MoveTheCar(string side)
    {
        if(side == "left" && !isCarMovingHorizontally)
        {
            isCarMovingHorizontally = true;
            car.transform.DOMoveX(Mathf.Clamp(car.transform.position.x - 65f, -65f, 45f), 0.5f).SetEase(ease);
            yield return new WaitForSeconds(0.5f);
            isCarMovingHorizontally = false;
        }
        if (side == "right" && !isCarMovingHorizontally)
        {
            isCarMovingHorizontally = true;
            car.transform.DOMoveX(Mathf.Clamp(car.transform.position.x + 65f, -65f, 65f), 0.5f).SetEase(ease);
            yield return new WaitForSeconds(0.5f);
            isCarMovingHorizontally = false;
        }
    }

    public void SpawnNextRoadPart(Transform givenTransform)
    {
        Instantiate(roadPrefab, new Vector3(0, givenTransform.position.y + 1400, 0), Quaternion.identity, parentRoad.transform);
    }

    public void DestroyPastRoadPart()
    {
        roadsTransforms = parentRoad.GetComponentsInChildren<Transform>();
        if(roadsTransforms.Length > 4)
        {
            for(int i = 5; i < roadsTransforms.Length; i++)
            {
                currentCarPosY = car.transform.position.y;
                if (roadsTransforms[i].position.y + 1200 < currentCarPosY)
                {
                    Destroy(roadsTransforms[i].gameObject);
                }
            }
        }
    }

    public void ShowBreakFX()
    {
        StartCoroutine(ShowBreakFX_Coroutine());
    }

    IEnumerator ShowBreakFX_Coroutine()
    {
        breakFXObject.transform.DOLocalMoveY(-4f, 0.3f);
        yield return new WaitForSeconds(1f);
        car.transform.GetChild(0).localPosition = Vector3.zero;
    }

    public void ShowFlameFX()
    {
        isFlameBeingShowed = true;
        StartCoroutine(ShowFlameFX_Coroutine());
    }

    IEnumerator ShowFlameFX_Coroutine()
    {
        flameFXObject.transform.DOLocalMoveY(-4.14f, 2f);
        yield return new WaitForSeconds(2f);
        isFlameBeingShowed = false;
    }

    public void HideFlameFX()
    {
        isFlameBeingShowed = false;
        StartCoroutine(HideFlameFX_Coroutine());
    }

    IEnumerator HideFlameFX_Coroutine()
    {
        flameFXObject.transform.DOLocalMoveY(0f, 2f);
        yield return new WaitForSeconds(2f);
        isFlameBeingShowed = true;
    }
}
