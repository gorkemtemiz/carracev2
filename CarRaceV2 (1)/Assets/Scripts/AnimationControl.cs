﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationControl : MonoBehaviour
{
    public void GoNextPage_carIntro()
    {
        UIControl.Instance.GoNextPage_carIntro();
    }

    public void PlaySFX_carIntro()
    {
        UIControl.Instance.introVideo_AS.Play();
    }
}
